package com.application.backend;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

public class DatabaseOperator {

	// Variable we will need for the whole process of creating queries, updates and
	// connecting
	//private static Connection connection;
	private Statement statement;
	private static ResultSet resultSet;
	private static PreparedStatement ps;

	
	//Normally "root"/"" user always exists, so for the beginning there is no need in creating a new one.
	//When the program is finished it should log on with the credentials given by the user - which then needs a user account created manually or automatically.
	String url = "jdbc:mysql://localhost:3306/nameOfDATABASE";
	String user = "root";
	String password = "";
	String dbName = "nameOfDATABASE";

	public Connection createConnectionToDatabase() {
		try {
			Connection con = DriverManager.getConnection(url, user, password);
			System.out.println("Successfully connected: " + con);
			return con;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public void pushTaskToDatabase(String taskName, Date date) {
		Connection con = createConnectionToDatabase();
		String query = "INSERT INTO `nameOfTable` (`value1`, `value2`) "
				+ "SELECT '" + taskName + "', '" + date + "' FROM DUAL "
				+ "WHERE NOT EXISTS (SELECT * FROM `table` "
				+ "WHERE `value1`='" + taskName + "' AND `value2`='" + date + "' LIMIT 1) ";
		closeAllConnectionsToDatabase(con);
	}
	
	private void closeAllConnectionsToDatabase(Connection connection) {

		if (resultSet != null) {
			try {
				resultSet.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else 
			System.out.println("\nResultSet closed");
		
		if (connection != null) {
			try {
				connection.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else 
			System.out.println("\nConnection closed");
		
		if (statement != null) {
			try {
				statement.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else 
			System.out.println("\nStatement closed");
		
		System.out.println("\n\nDisconnected\n");
	}

}
