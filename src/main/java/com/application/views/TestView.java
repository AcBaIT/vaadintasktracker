package com.application.views;

import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;


@SuppressWarnings("serial")
@PageTitle("Test")
@Route(value = "test", layout = MainLayout.class)
public class TestView extends VerticalLayout {
	
	TextField textField;
	
	public TestView() {
		TextField testText = new TextField("This is your test page");
		add(testText);
	}



}
