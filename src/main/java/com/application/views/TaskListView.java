package com.application.views;

import java.sql.Date;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import com.application.backend.DatabaseOperator;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

@SuppressWarnings("serial")
@PageTitle("New")
@Route(value = "new", layout = MainLayout.class)
public class TaskListView extends VerticalLayout {
	
	public ArrayList<Checkbox> list = new ArrayList<>();	

    public TaskListView() {
    	DatabaseOperator dataOp = new DatabaseOperator();
        setSpacing(false);

		VerticalLayout todosList = new VerticalLayout();
		TextField taskField = new TextField();
		Label HeadlineTasks = new Label();
		DatePicker dp = new DatePicker();
		Button addButton = new Button("Add");
		Button ListBooks = new Button("Secondary View");
		this.list = new ArrayList<>();
				
		dp.setPlaceholder("Date");
		taskField.setPlaceholder("Name Of Task");
		HeadlineTasks.setText("Tasklist:");
		
		addButton.addClickListener(click -> {
			Checkbox checkbox = new Checkbox(taskField.getValue() + " due: " 
		+ dp.getValue().format(DateTimeFormatter.ofPattern("yyyy.MM.dd")));
			this.list.add(checkbox);
			//This line pushes the tasks to its given database
			dataOp.pushTaskToDatabase(taskField.getValue().toString(), Date.valueOf(dp.getValue().toString()));
			todosList.add(checkbox);
		});

		addButton.addClickShortcut(Key.ENTER);
		
		add(
			new H1("Vaadin Todo"), 
			new H2(HeadlineTasks), 
			todosList, 
			new HorizontalLayout(
					taskField, 
					dp, 
					addButton)
//			ListBooks
			);
    }
}
